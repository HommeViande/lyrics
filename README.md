# Lyrics

This repo is a small hand-made list of `.lrc` files.


## Todo :

- Megadeth :
 * [x] Looking Down The Cross
 * [ ] Symphony of Destruction
 * [ ] High Speed Dirt
 * [ ] Architecture of Aggression
 * [ ] Foreclosure Of a Dream
 * [ ] Sweating Bullets
 * [ ] Countdown To Extinction
 * [ ] Captive Honour
 * [ ] Crown Of Worms
 * [x] Holy Wars... The Punishment Due
 * [ ] Hangar 18
 * [ ] Take No Prisoners
 * [ ] Poison Was The Cure
 * [ ] Lucretia
 * [ ] Tornado of Souls
 * [ ] Dawn Patrol
 * [ ] Rust In Peace... Polaris
 * [ ] Set The World Afire
 * [ ] Anarchy In The U.K.
 * [ ] Mary Jane
 * [ ] In My Darkest Hour
 * [ ] Hook In Mouth
 * [x] Wake Up Dead
 * [x] The Conjuring
 * [x] Peace Sells
 * [ ] Good Mourning / Black Fridey
 * [ ] My Last Words
 * [ ] Last Rites / Loved to Deth
 * [ ] Killing Is My Business... And Business Is Good
 * [ ] Rattlehead
 * [ ] Chosen Ones
 * [ ] Mechanix
 * [ ] Train of Consequences
 * [ ] A Tout Le Monde
 * [ ] Elysian Fields
 * [x] Youthanasia
 * [ ] Victory
 * [ ] Angry Again
 * [ ] Problems
 * [ ] New World Order
 * [ ] Trust
 * [ ] Almost Honest
 * [ ] Use The Man
 * [ ] A Secret Place
 * [x] Have Cool, Will Travel
 * [ ] She-Wolf
 * [ ] Disconnect
 * [ ] The World Needs a Hero
 * [x] Moto Psycho
 * [ ] 1000 Times Goodbye
 * [ ] Burning Bridges
 * [ ] Losing My Senses
 * [ ] Dread and the Fugitive Mind
 * [ ] Return to Hangar
 * [ ] Die Dead Enough
 * [ ] Sleepwalker
 * [ ] Washington Is Next!
 * [ ] Never Walk Alone... A Call To Armes
 * [ ] United Abominations
 * [ ] Play For Blood
 * [x] Amerikhastan
 * [ ] Burnt Ice
 * [ ] This Day We Fight!
 * [ ] Endgame
 * [ ] The Hardest Part of Letting Go... Sealed With a Kiss
 * [ ] The Right To Go Insane
 * [x] Public Enemy No. 1
 * [ ] Whose Life [Is It Anyways?]
 * [ ] 13
 * [ ] Kingmaker
 * [ ] Super Collider
 * [ ] Built For War
 * [x] Dance In The Rain
 * [ ] The Threat Is REal
 * [ ] Dystopia
 * [ ] Fatal Illusion

- Testament :
 * [ ] Over The Wall
 * [ ] Return To Serenity
 * [ ] Practice What You Preach
 * [ ] Trial By Fire
 * [ ] Disciples Of The Watch
 * [ ] Electric Crown
 * [ ] The New Order
 * [ ] Brotherhood of the Snake

- Exodus :
 * [ ] The Toxic Waltz
 * [ ] Blacklist

- Metallica :
 * [ ] One
 * [ ] Master Of Puppets
 * [ ] Fight Fire With Fire
 * [ ] Ride The Lightning
 * [ ] Whiplash

- Anthrax :
 * [ ] Madhouse
 * [ ] Indians
 * [ ] Got The Time
 * [ ] Antisocial
 * [ ] Fight 'Em 'Til You Can't

- Trust :
 * [ ] Antisocial
 * [ ] Démocrassie
 * [ ] Le gouvernement comme il respire
 * [ ] Instinct de mort
 * [ ] Au nom de la race
 * [ ] Saumur

- Motôrhead :
 * [ ] Sacrifice
 * [ ] Overkill
 * [ ] Ace of Spades

- Dio :
 * [ ] Holy Diver
 * [ ] Rainbow In The Dark

- Takahashi Brothers (Initial D) :
 * [ ] Dancing
 * [ ] Deja Vu
 * [ ] Running in the 90's
 * [ ] Night Of Fire
 * [ ] Gas Gas Gas

- Tom Kareen :
 * [ ] Si je perds la boule


## Author

Michael - @HommeViande - PAPER : `michael.paper@protonmail.com`


## Licence

I don't even know if sharing lyrics freely without restriction is strictly legal, so I wouldn't risk licencing this repo.
If you're confident enough in the legal status of lyrics, just do what the fuck you want with its content.
